import Vue from 'vue'
import Router from 'vue-router'
import login from "../views/login.vue"
import Main from "../views/main.vue"
import welcome from "../views/welcome.vue"
import User from "../views/User.vue"
import rights from "../views/rights.vue"
import Roles from "../views/Roles.vue"

Vue.use(Router)

let router=new Router({
  routes: [
    {
      path: '/',
      redirect: login
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/main',
      name: 'main',
      component: Main,
      children:[
        { path:"/welcome",component: welcome,},
        { path:"/Users",component: User,},
        {path:"/rights",component: rights,},
        {path:"/Roles",component: Roles, }
      ],
      redirect: "/welcome"
      
    }
  ]
})
router.beforeEach((to,from,next)=>{
  if(to.path==='/login'){
    next()
  }else{
    let curToken=window.sessionStorage.getItem('token')
    if(!curToken){
      next('/login')
    }else{
      next()
    }
  }

})
export default router