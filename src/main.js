// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import elementUi from "element-ui"
import "./assets/css/common.css"
import "./assets/fonts/iconfont.css"
import "element-ui/lib/theme-chalk/index.css"
import axios from "axios"
import VueAxios from "vue-axios"


Vue.config.productionTip = false

Vue.use(elementUi);

Vue.use(VueAxios,axios)

axios.interceptors.request.use(config=>{
  config.headers.Authorization = window.sessionStorage.getItem("token")
  return config
})
axios.interceptors.response.use(info=>{
  return info.data
})
axios.defaults.baseURL = "http://127.0.0.1:8888/api/private/v1/"
/* eslint-disable no-new */
Vue.prototype.$axios=axios

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
